package avecPattern;

public abstract class ReservationOuverte extends Annulable{

	@Override
	public abstract void annuler();
	
	public abstract void clotureReservation();
}
