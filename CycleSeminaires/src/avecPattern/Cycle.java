package avecPattern;

import java.util.ArrayList;
import java.util.List;

public abstract class Cycle {
	
	protected Etat etatCourant;
	
	protected String titre;
	protected String resume;
	protected String creneaux;
	protected int capacite;
	
	protected int nbInscrit;

	protected List<String> inscrits;
	protected List<String> listeAttentes;

	public Cycle(){
		this.nbInscrit = 0;
		this.inscrits = new ArrayList<>(nbInscrit);
		this.setEtatCourant(new Planification());
	}
	
	public void requete(){
		etatCourant.requete();
	}
	
	
	public Etat getEtatCourant() {
		return etatCourant;
	}

	public void setEtatCourant(Etat etatCourant) {
		this.etatCourant = etatCourant;
	}
	
	/**
	 * Getter et setter
	 */
	
	public int getNbInscrit() {
		return nbInscrit;
	}

	public List<String> getInscrit() {
		return inscrits;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getCreneaux() {
		return creneaux;
	}

	public void setCreneaux(String creneaux) {
		this.creneaux = creneaux;
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
}
