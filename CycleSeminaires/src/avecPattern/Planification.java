package avecPattern;

import java.util.Scanner;

public class Planification extends Annulable{

	@Override
	public void annuler() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requete() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Veuillez entrer le titre du cycle svp: \n");
		setTitre(scan.nextLine());
		System.out.println("Entrer le resume svp \n");
		setResume(scan.nextLine());
		System.out.println("Entrer le creno\n");
		setCreneuaux(scan.nextLine());
		System.out.println("Enfin la capacite\n");
		setCapacite(scan.nextInt());
		System.out.println("Ouverture de la reservation...");
		ouvertureResa();
	}
	
	public void setTitre(String titre){
		this.cycle.setTitre(titre);
	}
	
	public void setResume(String resume){
		this.cycle.setResume(resume);
	}
	
	public void setCreneuaux(String creno){
		this.cycle.setCreneaux(creno);
	}
	
	public void setCapacite(int capa){
		this.cycle.setCapacite(capa);
	}
	
	public void ouvertureResa(){
		cycle.setEtatCourant(new PlaceDispo());
	}
}
