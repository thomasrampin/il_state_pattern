package avecPattern;

public abstract class Annulable extends Etat{
	
	public abstract void annuler();
	public abstract void requete();
}
